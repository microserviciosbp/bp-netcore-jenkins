FROM 10.207.180.10:5000/net-core:3.0 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM 10.207.180.10:5000/aspnet:2.2 as runtime

EXPOSE 8080
LABEL io.k8s.description="Test Netcore for BP" \
      io.k8s.display-name="TEST JENKINS .NET Core 2.1" \
      io.openshift.tags="runtime,.net,dotnet,dotnetcore,rh-dotnet21-runtime" \
      io.openshift.expose-services="8080:http"

ENV ASPNETCORE_URLS=http://*:8080

WORKDIR /app
RUN ls
COPY --from=build-env /app/out .
ENV ASPNETCORE_URLS=http://*:8080
ENTRYPOINT ["dotnet", "bp-netcore-jenkins.dll"]
USER 1001